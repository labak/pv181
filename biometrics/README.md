10 Biometrics 1: Biometrics intro, face recognition
===================================================

## Virtual machines

There are 2 virtual machines prepared:
* headless version, ready for `vagrant ssh` use
* GUI version with Unity and GIMP installed
Credentials for both: login `vagrant`, password `vagrant`

## Building the virtual machine

1. Install VirtualBox. (https://www.virtualbox.org/wiki/Downloads)
2. Install Vagrant. (https://www.vagrantup.com/downloads.html)
3. Clone this repo. (`git clone https://gitlab.fi.muni.cz/labak/pv181.git`)
4. Open terminal/PowerShell/cmd and navidate to downloaded repository, exercise 10.
5. Let Vagrant set up the virtual machine. (`vagrant up`)
6. Wait till the machine updates and OpenBR gets installed,

## Using OpenBR with virtual machine

1. Open terminal/PowerShell/cmd and navigate to this repo, exercise 10.
2. Run Vagrant built Vagrant machine. (`vagrant up`)
  * If the setup scrit is interrupted or you need to run it again, use `vagrant provision`.
3. Log into the machine. (`vagrant ssh`)
4. Navigate to `/vagrant` -- this folder is synchronized with excercise 10 root on host machine. (`cd /vagrant`)
5. Use OpenBR with input prepared on host machine in the synced folder.
6. Afterwards, shutdown the machine using `vagrant halt` or delete with `vagrant destroy`.

## OpenBR web examples

Examples are taken from http://openbiometrics.org/

* **Face recognition:** `br -algorithm FaceRecognition -compare me.jpg you.jpg`
* **Age estimation:** `br -algorithm AgeEstimation -enroll me.jpg you.jpg metadata.csv`
* **Gender estimation:** `br -algorithm GenderEstimation -enroll me.jpg you.jpg metadata.csv`
