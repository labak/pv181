#!/bin/bash

# Bootstrap script to setup OpenBR

# Prepare environment
apt -y update
apt -y install build-essential
apt -y install zip unzip
apt -y install cmake cmake-curses-gui
cd ~
mkdir -p download
cd download

# Download, build and install OpenCV
which opencv_createsamples
RET=$?
if [ $RET -eq 0 ]
then
	>&2 echo "==== OpenCV already installed, skipping ===="
else
	cd ~/download
	rm -rf opencv
	mkdir opencv
	cd opencv
	wget http://sourceforge.net/projects/opencvlibrary/files/opencv-unix/2.4.11/opencv-2.4.11.zip
	unzip opencv-2.4.11.zip
	cd opencv-2.4.11
	mkdir build
	cd build
	cmake -DCMAKE_BUILD_TYPE=Release ..
	make -j4
	make install
	cd ../..
	rm -rf opencv-2.4.11*
fi

# Download, build and install Qt 5.4.1
apt -y install qt5-default libqt5svg5-dev qtcreator

# Download, build and install OpenBR
which br
RET=$?
if [ $RET -eq 0 ]
then
	>&2 echo "==== OpenBR already installed, skipping ===="
else
	cd ~/download
	rm -rf openbr
	git clone https://github.com/biometrics/openbr.git
	cd openbr
	git checkout v1.1.0
	git submodule init
	git submodule update
	mkdir build
	cd build
	cmake -DCMAKE_BUILD_TYPE=Release ..
	make -j4
	make install
fi

>&2 echo "==== OpenBR provision script complete ===="
