PV181 Laboratory of security and applied cryptography
=====================================================

## Syllabus for autumn 2017

| # | Topic | Teachers |
| --- | --- | --- |
| 1 | Organization + OpenSSL1(tool) - Symmetric crypto | Marek Sýs |
| 2 | OpenSSL1(tool) - Asymetric crypto | Marek Sýs |
| 3 | Dig Signature + certificates | Marek Sýs |
| 4 | ASN1/ASN1 crypto | Marek Sýs |
| 5 | OpenSSL2(lib) | Milan Brož |
| 6 | Other libs | Milan Brož |
| 7 | JAVA crypto | Dušan Klinec |
| 8 | MS Crypto API | Petr Mornfall Ročkai |
| 9 | Biometrics1: face, finger, SDK | Martin Ukrop, Vlasta Šťavová |
| 10 | Biometrics2: fake fingerprint, signature dynamics | Martin Ukrop, Vlasta Šťavová |
| 11 | Standards | Zdeněk Říha |
| 12 | GnuTLS / Key Vault. | Petr Mornfall Ročkai |
| 13 | Finalize projects,... | Marek Sýs |

